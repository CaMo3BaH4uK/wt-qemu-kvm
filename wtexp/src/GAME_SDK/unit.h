//
// Created by camo3bah4uk on 05.06.22.
//

#ifndef WTEXP_UNIT_H
#define WTEXP_UNIT_H

#include "string"

#include "memory.h"
#include "offsets.h"
#include "datatypes.h"


class Unit {
private:
    // Funcs
    uint64_t GetUnitInfo();

    // Vars
    Memory *client_memory;
    uint64_t unit_base;
public:
    // Funcs
    Unit(Memory *client_memory, uint64_t unit_base);
    Vector3 GetUnitPos();
    Matrix3x3 GetUnitRotation();
    float* GetBBMax();
    int GetFlags();
    std::string GetUnitType();
    std::string GetUnitName();
};


#endif //WTEXP_UNIT_H
