//
// Created by camo3bah4uk on 05.06.22.
//

#include "unit.h"

Unit::Unit(Memory *client_memory, uint64_t unit_base) {
    this->client_memory = client_memory;
    client_memory->Read<uint64_t>(unit_base, this->unit_base);
}

Vector3 Unit::GetUnitPos() {
    Vector3 unit_pos;
    client_memory->Read<Vector3>(this->unit_base + OFFSETS::pos_offset, unit_pos);
    return unit_pos;
}

Matrix3x3 Unit::GetUnitRotation() {
    Matrix3x3 unit_rotation;
    client_memory->Read<Matrix3x3>(this->unit_base + OFFSETS::rotation_offset, unit_rotation);
    return unit_rotation;
}

float *Unit::GetBBMax() {
    float *BBMax = new float[6];
    client_memory->ReadArray<float>(this->unit_base + OFFSETS::bbmax_offset, BBMax, 6);
    return BBMax;
}

int Unit::GetFlags() {
    int flags;
    client_memory->Read<int>(this->unit_base + OFFSETS::unit_flags, flags);
    return flags;
}

uint64_t Unit::GetUnitInfo() {
    uint64_t unit_info;
    client_memory->Read<uint64_t>(this->unit_base + OFFSETS::unit_info, unit_info);
    return unit_info;
}

std::string Unit::GetUnitType() {
    std::string unit_type;
    uint64_t unit_type_p;
    client_memory->Read<uint64_t>(this->GetUnitInfo() + 0x28, unit_type_p);
    client_memory->ReadString(unit_type_p, unit_type);
    return unit_type;
}

std::string Unit::GetUnitName() {
    std::string unit_name;
    uint64_t unit_name_p;
    client_memory->Read<uint64_t>(this->GetUnitInfo() + 0x18, unit_name_p);
    client_memory->ReadString(unit_name_p, unit_name);
    return unit_name;
}
