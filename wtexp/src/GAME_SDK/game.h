//
// Created by camo3bah4uk on 04.06.22.
//

#ifndef WTEXP_GAME_H
#define WTEXP_GAME_H


#include <cstdint>
#include <thread>

#include "memory.h"
#include "player.h"
#include "datatypes.h"
#include "offsets.h"

class Game {
private:
    // Funcs
    uint64_t GetPlayersList();

    // Vars;
    Memory client_memory;
    const char* client_proc = "aces.exe";
    uint64_t game_base;
public:
    // Funcs
    void Init();
    int GetPlayersCount();
    Player GetPlayerByIndex(int index);
    Player GetLocalPlayer();
    Matrix4x4 GetViewMatrix();

    // Vars
};


#endif //WTEXP_GAME_H
