cmake_minimum_required(VERSION 3.22)
project(wtexp)

set(CMAKE_CXX_STANDARD 20)

find_package(Threads)

# glfw
set(GLFW_DIR glfw)
add_subdirectory(${GLFW_DIR})
include_directories(${GLFW_DIR}/include)
include_directories(${GLFW_DIR}/deps)

# imgui
set(IMGUI_DIR imgui)
include_directories(${IMGUI_DIR} ${IMGUI_DIR}/backends ..)

include_directories(imgui)
include_directories(imgui/backends)

# memflow
set(MEMFLOW_DIR memflow_lib)
include_directories(memflow_lib/memflow-win32-ffi/)
include_directories(memflow_lib/memflow-ffi/)
configure_file(/home/camo3bah4uk/.local/lib/memflow/libmemflow_kvm.so ${CMAKE_CURRENT_BINARY_DIR}/libmemflow_kvm.so COPYONLY)

# SDKs
include_directories(src/DMA_SDK)
include_directories(src/GAME_SDK)

# create binary from /src
file(GLOB_RECURSE wtexp-cpp-sources "src/*.cpp")
file(GLOB_RECURSE wtexp-sources "src/*.c")
file(GLOB_RECURSE wtexp-headers "src/*.h")

add_executable(
        wtexp
        ${wtexp-sources}
        ${wtexp-cpp-sources}
        ${wtexp-headers}
        ${IMGUI_DIR}/backends/imgui_impl_glfw.cpp
        ${IMGUI_DIR}/backends/imgui_impl_opengl3.cpp
        ${IMGUI_DIR}/imgui.cpp
        ${IMGUI_DIR}/imgui_draw.cpp
        ${IMGUI_DIR}/imgui_demo.cpp
        ${IMGUI_DIR}/imgui_tables.cpp
        ${IMGUI_DIR}/imgui_widgets.cpp src/DMA_SDK/memory.cpp src/DMA_SDK/memory.h src/GAME_SDK/game.cpp src/GAME_SDK/game.h src/DMA_SDK/datatypes.h src/GAME_SDK/player.cpp src/GAME_SDK/player.h src/GAME_SDK/unit.cpp src/GAME_SDK/unit.h src/GAME_SDK/offsets.cpp src/GAME_SDK/offsets.h)

target_compile_definitions(wtexp PRIVATE DEBUG)


target_link_libraries(wtexp
        GL
        glfw
        X11
        ${CMAKE_THREAD_LIBS_INIT}
        ${CMAKE_DL_LIBS}
        ${CMAKE_CURRENT_SOURCE_DIR}/${MEMFLOW_DIR}/target/release/libmemflow_win32_ffi.a)

target_include_directories(
        wtexp PUBLIC)