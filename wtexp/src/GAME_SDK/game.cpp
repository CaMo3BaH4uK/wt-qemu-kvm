//
// Created by camo3bah4uk on 04.06.22.
//

#include "game.h"


void Game::Init() {
    while (true) {
        if(this->client_memory.get_proc_status() != process_status::FOUND_READY)
        {

            std::this_thread::sleep_for(std::chrono::seconds(1));
#ifdef DEBUG
            printf("Searching for client process...\n");
#endif
            this->client_memory.open_proc(this->client_proc);

            if(this->client_memory.get_proc_status() == process_status::FOUND_READY)
            {
                this->game_base = this->client_memory.get_proc_baseaddr();
#ifdef DEBUG
                printf("\nClient process found\n");
                printf("Base: %lx\n", this->game_base);
                uint64_t test_reading;
                this->client_memory.Read<uint64_t>(this->game_base, test_reading);
                printf("Test reading: %lx\n", test_reading);
#endif
                break;
            }
        }
        else
        {
            this->client_memory.check_proc();
        }
    }
}

int Game::GetPlayersCount() {
    int players_count;
    this->client_memory.Read<int>(this->game_base + OFFSETS::player_count_offset, players_count);
    return players_count;
}

uint64_t Game::GetPlayersList() {
    uint64_t player_list;
    this->client_memory.Read<uint64_t>(this->game_base + OFFSETS::player_list_offset, player_list);
    return player_list;
}

Player Game::GetPlayerByIndex(int index) {
    return Player(&(this->client_memory), this->GetPlayersList() + 0x8 + 0x10*index);
}

Player Game::GetLocalPlayer() {
    return Player(&(this->client_memory), this->game_base + OFFSETS::local_player_offset);
}

Matrix4x4 Game::GetViewMatrix() {
    Matrix4x4 view_matrix;
    uint64_t game;
    this->client_memory.Read<uint64_t>(this->game_base + OFFSETS::game_offset, game);
    this->client_memory.Read<Matrix4x4>(game + OFFSETS::game_matrix_offset, view_matrix);
    return view_matrix;
}

