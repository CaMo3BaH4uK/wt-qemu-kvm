//
// Created by camo3bah4uk on 04.06.22.
//

#ifndef WTEXP_PLAYER_H
#define WTEXP_PLAYER_H

#include "memory.h"
#include "unit.h"
#include "offsets.h"

#include "string"

class Player {

private:
    // Funcs

    // Vars
    Memory *client_memory;
    uint64_t player_base;
public:
    // Funcs
    Player(Memory *client_memory, uint64_t player_base);
    std::string GetPlayerName();
    uint8_t GetPlayerTeam();
    Unit GetPlayerUnit();


    // Vars
};


#endif //WTEXP_PLAYER_H
