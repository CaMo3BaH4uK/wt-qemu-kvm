//
// Created by camo3bah4uk on 04.06.22.
//

#include "player.h"


Player::Player(Memory *client_memory, uint64_t player_base) {
    this->client_memory = client_memory;
    client_memory->Read<uint64_t>(player_base, this->player_base);
}

std::string Player::GetPlayerName() {
    std::string player_name;
    client_memory->ReadString(this->player_base + OFFSETS::player_name_offset, player_name);
    return player_name;
}

uint8_t Player::GetPlayerTeam() {
    uint8_t player_team;
    client_memory->Read<uint8_t>(this->player_base + OFFSETS::player_team_offset, player_team);
    return player_team;
}

Unit Player::GetPlayerUnit() {
    return Unit(this->client_memory, this->player_base + OFFSETS::unit_control_offset);
}
