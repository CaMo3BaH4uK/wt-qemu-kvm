//
// Created by camo3bah4uk on 05.06.22.
//

#ifndef WTEXP_OFFSETS_H
#define WTEXP_OFFSETS_H


#include <cstdint>
namespace OFFSETS {
    extern uint64_t game_offset;
    extern uint64_t game_matrix_offset;

    extern uint64_t player_list_offset;
    extern uint64_t player_count_offset; //player_list_offset + 0x8
    extern uint64_t player_index_offset;
    extern uint64_t player_name_offset;
    extern uint64_t player_team_offset;

    extern uint64_t local_player_offset; //player_list_offset + 0x20

    extern uint64_t unit_control_offset;
    extern uint64_t unit_state;
    extern uint64_t unit_flags;
    extern uint64_t unit_team;
    extern uint64_t unit_info;

    extern uint64_t pos_offset;
    extern uint64_t rotation_offset;
    extern uint64_t bbmin_offset;
    extern uint64_t bbmax_offset;
}


#endif //WTEXP_OFFSETS_H
