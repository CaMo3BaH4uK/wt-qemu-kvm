//
// Created by camo3bah4uk on 05.06.22.
//

#include "offsets.h"

namespace OFFSETS {
    uint64_t game_offset = 0x3E283F0;
    uint64_t game_matrix_offset = 0x740;

    uint64_t player_list_offset = 0x3E8F0A0;
    uint64_t player_count_offset = player_list_offset + 0x8; //player_list_offset + 0x8. Player = player_list  + 0x8 + 0x10*index
    uint64_t player_index_offset = 0x8;
    uint64_t player_name_offset = 0xB0;
    uint64_t player_team_offset = 0x0208;

    uint64_t local_player_offset = 0x3E8F180;

    uint64_t unit_control_offset = 0x710;
    uint64_t unit_state = 0xFF0;
    uint64_t unit_flags = 0x1038;
    uint64_t unit_team = 0x1048;
    uint64_t unit_info = 0x10F0;

    uint64_t pos_offset = 0x8E8;
    uint64_t rotation_offset = 0x8C4;
    uint64_t bbmin_offset = 0x208;
    uint64_t bbmax_offset = 0x214;
}