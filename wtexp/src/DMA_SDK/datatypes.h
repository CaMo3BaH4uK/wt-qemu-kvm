//
// Created by camo3bah4uk on 04.06.22.
//

#ifndef WTEXP_DATATYPES_H
#define WTEXP_DATATYPES_H

#include <cmath>
#include <cstdint>

struct Vector3 {
    float x, y, z;

    Vector3() {};

    Vector3(float x_, float y_, float z_) : x(x_), y(y_), z(z_) {}

    float magnitude() {
        return sqrt(x * x + y * y + z * z);
    }

    float dist(Vector3& to) {
        Vector3 diff = *this - to;
        return diff.magnitude();
    }

    void reset() {
        x = 0;
        y = 0;
        z = 0;
    }

    Vector3 operator-(const Vector3& vec) {
        return { x - vec.x, y - vec.y, z - vec.z };
    }

    Vector3 operator+(const Vector3& vec) {
        return { x + vec.x, y + vec.y, z + vec.z };
    }

    Vector3 operator*(const float& val) {
        return Vector3(x * val, y * val, z * val);
    }

    Vector3 operator/(const float& val) {
        return Vector3(x / val, y / val, z / val);
    }

    float Dot(Vector3& other) {
        return (x * other.x + y * other.y + z * other.z);
    }

    float Length() {
        return sqrt(x * x + y * y + z * z);
    }

    void Normalize() {

        while (x < -180.0f) x += 360.0f;
        while (x > 180.0f) x -= 360.0f;

        while (y < -180.0f) y += 360.0f;
        while (y > 180.0f) y -= 360.0f;

        if (x > 89.0f) x = 89.0f;
        else if (x < -89.0f) x = -89.0f;

        if (y > 180.0f) y = 180.0f;
        else if (y < -180.0f) y = -180.0f;

        z = 0.f;
    }

};


struct Vector2 {
    float x, y;

    const Vector2 operator-(const Vector2& vec) {
        return {x - vec.x, y - vec.y};
    }

    const Vector2 operator+(const Vector2& vec) {
        return {x + vec.x, y + vec.y};
    }
};

struct Matrix3x3 {
    float matrix[3][3];
    float* operator[ ](int idx) {
        return matrix[idx];
    }
};

struct Matrix4x4 {
    float matrix[4][4];
    float* operator[ ](int idx) {
        return matrix[idx];
    }
};


#endif //WTEXP_DATATYPES_H
