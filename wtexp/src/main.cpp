#include <iostream>
#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"
#include <cstdio>
#include <GLFW/glfw3.h>
#include <X11/Xlib.h>
#include <X11/keysym.h>
#include <chrono>
#include <thread>

#include "game.h"
#include "player.h"
#include "unit.h"

#pragma clang diagnostic push
#pragma ide diagnostic ignored "UnreachableCode"
#pragma region GLFWError
static void glfw_error_callback(int error, const char* description) {
    fprintf(stderr, "Glfw Error %d: %s\n", error, description);
}
#pragma endregion

Game game;

int screen_x, screen_y, screen_adj_x = 20, screen_adj_y = 0, screen_adj_w, screen_adj_h;

bool WorldToScreen(Vector3 worldPosition, ImVec2& screenPosition, Matrix4x4 viewmatrix)
{
    float* matrix = &(viewmatrix[0][0]);

    float cW = worldPosition.x * matrix[3] + worldPosition.y * matrix[7] + worldPosition.z * matrix[11] + matrix[15];

    bool visible = cW >= 0.1f;
    if (!visible)
        cW = -1.0 / cW;
    else
        cW = 1.0 / cW;

    float cX = worldPosition.x * matrix[0] + worldPosition.y * matrix[4] + worldPosition.z * matrix[8] + matrix[12];
    float cY = worldPosition.x * matrix[1] + worldPosition.y * matrix[5] + worldPosition.z * matrix[9] + matrix[13];
    float cZ = worldPosition.x * matrix[2] + worldPosition.y * matrix[6] + worldPosition.z * matrix[10] + matrix[14];

    float nx = cX * cW;
    float ny = cY * cW;
    float nz = cZ * cW;

    screenPosition.x = (1920 / 2 * nx) + (nx + 1920 / 2);
    screenPosition.y = -(1080 / 2 * ny) + (ny + 1080 / 2);

    return visible;
}

void ModelDebugBox(Vector3 position, Matrix3x3 rotation, float* scale, Matrix4x4 ViewMatrix, ImDrawList* draw_list)
{

    Vector3 ax[6];
    ax[0] = Vector3(rotation.matrix[0][0], rotation.matrix[0][1], rotation.matrix[0][2]) * scale[0];
    ax[1] = Vector3(rotation.matrix[1][0], rotation.matrix[1][1], rotation.matrix[1][2]) * scale[1];
    ax[2] = Vector3(rotation.matrix[2][0], rotation.matrix[2][1], rotation.matrix[2][2]) * scale[2];
    ax[3] = Vector3(rotation.matrix[0][0], rotation.matrix[0][1], rotation.matrix[0][2]) * scale[3];
    ax[4] = Vector3(rotation.matrix[1][0], rotation.matrix[1][1], rotation.matrix[1][2]) * scale[4];
    ax[5] = Vector3(rotation.matrix[2][0], rotation.matrix[2][1], rotation.matrix[2][2]) * scale[5];

    Vector3 temp[6];
    temp[0] = position + ax[2];
    temp[1] = position + ax[5];
    temp[2] = temp[0] + ax[3];
    temp[3] = temp[1] + ax[3];
    temp[4] = temp[0] + ax[0];
    temp[5] = temp[1] + ax[0];

    Vector3 v[8];
    v[0] = temp[2] + ax[1];
    v[1] = temp[2] + ax[4];
    v[2] = temp[3] + ax[4];
    v[3] = temp[3] + ax[1];
    v[4] = temp[4] + ax[1];
    v[5] = temp[4] + ax[4];
    v[6] = temp[5] + ax[4];
    v[7] = temp[5] + ax[1];

    ImVec2 p1, p2;

    float coef_x = (screen_adj_w - screen_adj_x)/1920.0f;
    float coef_y = (screen_adj_h - screen_adj_y)/1080.0f;



    for(int i = 0; i < 4; i++)
    {
        WorldToScreen(v[i], p1, ViewMatrix);
        WorldToScreen(v[(i + 1) & 3], p2, ViewMatrix);
        p1.x *= coef_x;
        p1.y *= coef_y;
        p2.x *= coef_x;
        p2.y *= coef_y;
        p1.x += screen_adj_x;
        p1.y += screen_adj_y;
        p2.x += screen_adj_x;
        p2.y += screen_adj_y;
        draw_list->AddLine(p1, p2, ImColor(0.0f, 0.0f, 1.0f, 1.0f));

        WorldToScreen(v[4 + i], p1, ViewMatrix);
        WorldToScreen(v[4 + ((i + 1) & 3)], p2, ViewMatrix);
        p1.x *= coef_x;
        p1.y *= coef_y;
        p2.x *= coef_x;
        p2.y *= coef_y;
        p1.x += screen_adj_x;
        p1.y += screen_adj_y;
        p2.x += screen_adj_x;
        p2.y += screen_adj_y;
        draw_list->AddLine(p1, p2, ImColor(1.0f, 0.0f, 0.0f, 1.0f));

        WorldToScreen(v[i], p1, ViewMatrix);
        WorldToScreen(v[4 + i], p2, ViewMatrix);
        p1.x *= coef_x;
        p1.y *= coef_y;
        p2.x *= coef_x;
        p2.y *= coef_y;
        p1.x += screen_adj_x;
        p1.y += screen_adj_y;
        p2.x += screen_adj_x;
        p2.y += screen_adj_y;
        draw_list->AddLine(p1, p2, ImColor(0.0f, 0.0f, 1.0f, 1.0f));
    }
}

int main(int argc, char *argv[]) {

    if(geteuid() != 0)
    {
        printf("Error: %s is not running as root\n", argv[0]);
        return 0;
    }

    game.Init();

#pragma region GLFW INIT
    // Window enabler
    bool MousePass = true;

    // Setup keyboard catcher
    Display* dpy = XOpenDisplay(NULL);
    char keys_return[32];

    // Setup window
    glfwSetErrorCallback(glfw_error_callback);
    if (!glfwInit())
        return 1;

    const char* glsl_version = "#version 130";
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

    glfwWindowHint(GLFW_TRANSPARENT_FRAMEBUFFER, 1);
    glfwWindowHint(GLFW_FOCUS_ON_SHOW, 1);
    glfwWindowHint(GLFW_MOUSE_PASSTHROUGH, 1);
    glfwWindowHint(GLFW_FLOATING, 1);
    glfwWindowHint(GLFW_MAXIMIZED, 1);

    //glfwWindowHint(GLFW_DECORATED, 0);

    // Create window with graphics context
    GLFWwindow* window = glfwCreateWindow(1920, 1080, "Wednesday", NULL, NULL);

    if (window == NULL)
        return 1;
    glfwMakeContextCurrent(window);


    if (!glfwInit())
        return 1;

    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;
    ImGui::StyleColorsDark();

    // Setup Platform/Renderer backends
    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init(glsl_version);

    ImVec4 clear_color = ImVec4(0.0f, 0.0f, 0.0f, 0.0f);

    glfwSetWindowAttrib(window, GLFW_MOUSE_PASSTHROUGH, MousePass);
    glfwSetWindowAttrib(window, GLFW_MAXIMIZED, 1);


    glfwGetWindowSize(window, &screen_x, &screen_y);
    screen_adj_w = 1827;
    screen_adj_h = screen_y;
#pragma endregion
    while (!glfwWindowShouldClose(window))
    {
#pragma region GLFW TICK
        glfwPollEvents();
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();
        ImGui::SetNextWindowPos(ImVec2(0, 0));
#pragma endregion

        ImGui::Begin("Draw worker", NULL, ImGuiWindowFlags_NoBackground | ImGuiWindowFlags_NoTitleBar);
        ImDrawList* draw_list = ImGui::GetBackgroundDrawList();
        //draw_list->AddLine(ImVec2(0,0), ImVec2(330,330), ImGui::GetColorU32(ImVec4(0.5, 0.5, 0.5, 1)), 4);


#pragma region KEYBOARD CATCHER
        XQueryKeymap( dpy, keys_return );
        KeyCode kc2 = XKeysymToKeycode( dpy, XK_Insert );
        bool bInsertPressed = !!( keys_return[ kc2>>3 ] & ( 1<<(kc2&7) ) );
        if (bInsertPressed) {
            MousePass = !MousePass;
            glfwSetWindowAttrib(window, GLFW_MOUSE_PASSTHROUGH, MousePass);
            std::this_thread::sleep_for(std::chrono::milliseconds(150));
        }
#pragma endregion

        if (true) {
            int players_count = game.GetPlayersCount();
            Player local_player = game.GetLocalPlayer();
            Unit local_player_unit = local_player.GetPlayerUnit();
            auto local_player_pos = local_player_unit.GetUnitPos();
            auto local_player_team = local_player.GetPlayerTeam();
            Matrix4x4 ViewMatrix = game.GetViewMatrix();
            for (int i = 0; i < players_count; ++i) {
                Player player = game.GetPlayerByIndex(i);
                if (player.GetPlayerTeam() == local_player_team) continue;
                Unit player_unit = player.GetPlayerUnit();
                if (player_unit.GetFlags() > 10000000) continue;
                auto unit_pos = player_unit.GetUnitPos();
                auto bbmax = player_unit.GetBBMax();
                auto rotation = player_unit.GetUnitRotation();
                ModelDebugBox(unit_pos, rotation, bbmax, ViewMatrix, draw_list);
                delete bbmax;

                ImVec2 origin = { };
                if (WorldToScreen(unit_pos, origin, ViewMatrix) && unit_pos.dist(local_player_pos) < 2800)
                {
                    char text[100];
                    sprintf(text, "%s %d m", player_unit.GetUnitName().c_str(), (int)unit_pos.dist(local_player_pos));

                    const auto size = ImGui::CalcTextSize(text);

                    float coef_x = (screen_adj_w - screen_adj_x)/1920.0f;
                    float coef_y = (screen_adj_h - screen_adj_y)/1080.0f;

                    origin.x *= coef_x;
                    origin.y *= coef_y;
                    origin.x += screen_adj_x;
                    origin.y += screen_adj_y;

                    draw_list->AddRectFilled({ origin.x - (size.x * 0.5f) - 5, origin.y + 5},
                                        { origin.x + (size.x * 0.5f) + 5, origin.y + 10 + (size.y * 0.5f) + 5 }, ImColor(0, 0, 0, 150));
                    draw_list->AddText({ origin.x - (size.x * 0.5f), origin.y + (size.y * 0.5f) },
                                  ImColor(255, 255, 255),
                                  text);
                }

//                ImVec2 screen_size = {1920, 1080};
//                ImVec2 p1, p2, p3;
//                float coef_x = (screen_adj_w - screen_adj_x)/screen_size.x;
//                float coef_y = (screen_adj_h - screen_adj_y)/screen_size.y;
//                WorldToScreen(unit_pos, p1, ViewMatrix, screen_size);
//                p1.x *= coef_x;
//                p1.y *= coef_y;
//                p2 = p1;
//                p3 = p1;
//                p3.y += 10;
//                p1.x += 20;
//                p2.x -= 20;
//                draw_list->AddLine(p1, p2, ImColor(1.0f, 0.0f, 0.0f, 1.0f), 4);
//                ImFont* font_current = ImGui::GetFont();
//                char buffer[100];
//                sprintf(buffer, "%s %d", player_unit.GetUnitName().c_str(), (int)unit_pos.dist(local_player_pos));
//                draw_list->AddText(font_current, 14, p3, ImColor(0.0f, 0.0f, 1.0f, 1.0f), buffer);
            }
        }




        ImGui::End();

//        if (!MousePass)
//            ImGui::ShowDemoWindow();

        // 2. Show a simple window that we create ourselves. We use a Begin/End pair to created a named window.
        if(!MousePass){
            ImGui::Begin("KVM Dude", NULL, ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_AlwaysAutoResize);
            ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
            ImGui::Spacing();

            if (ImGui::CollapsingHeader("Players"))
            {
                uint8_t local_player_team = game.GetLocalPlayer().GetPlayerTeam();
                int players_count = game.GetPlayersCount();
                for (int row = 0; row < players_count; row++)
                {
                    Player player = game.GetPlayerByIndex(row);
                    Unit player_unit = player.GetPlayerUnit();
                    char buffer[100];
                    sprintf(buffer, "%s %s %s %s", player.GetPlayerTeam() == local_player_team ? "Friend" : "Enemy",
                            player.GetPlayerName().c_str(),
                            player_unit.GetUnitName().c_str(),
                            player_unit.GetFlags() > 10000000 ? "Dead" : "Alive");
                    if (ImGui::CollapsingHeader(buffer))
                    {
                        if (ImGui::CollapsingHeader("Position"))
                        {
                            auto pos = player_unit.GetUnitPos();
                            ImGui::Text("X %f", pos.x);
                            ImGui::Text("Y %f", pos.y);
                            ImGui::Text("Z %f", pos.z);
                        }
                        if (ImGui::CollapsingHeader("BBMax"))
                        {
                            auto bbmax = player_unit.GetBBMax();
                            ImGui::Text("%f %f", bbmax[0], bbmax[1]);
                            ImGui::Text("%f %f", bbmax[2], bbmax[3]);
                            ImGui::Text("%f %f", bbmax[4], bbmax[5]);
                            delete bbmax;
                        }
                    }
                }
            }

            if (ImGui::CollapsingHeader("Screen settings"))
            {
                ImGui::InputInt("X", &screen_adj_x);
                ImGui::InputInt("Y", &screen_adj_y);
                ImGui::InputInt("W", &screen_adj_w);
                ImGui::InputInt("H", &screen_adj_h);
                if (screen_adj_x < 0) screen_adj_x = 0;
                if (screen_adj_x > screen_x-1) screen_adj_x = screen_x-1;
                if (screen_adj_y < 0) screen_adj_y = 0;
                if (screen_adj_y > screen_y-1) screen_adj_y = screen_y-1;
                if (screen_adj_w < 0) screen_adj_w = 0;
                if (screen_adj_w > screen_x-1) screen_adj_w = screen_x-1;
                if (screen_adj_h < 0) screen_adj_h = 0;
                if (screen_adj_h > screen_y-1) screen_adj_h = screen_y-1;

                draw_list->AddRect(ImVec2(screen_adj_x, screen_adj_y), ImVec2(screen_adj_w, screen_adj_h), ImColor(1.0f, 1.0f, 1.0f, 1.0f));
            }

            if (ImGui::CollapsingHeader("ViewMatrix Adjust"))
            {
                uint32_t step = 0x4;
                ImGui::InputScalar("Offset", ImGuiDataType_U32,    &OFFSETS::game_matrix_offset, &step, NULL, "%08X", ImGuiInputTextFlags_CharsHexadecimal);
            }

            ImGui::End();
        }


        // Rendering
#pragma region GLFW RENDER
        ImGui::Render();
        int display_w, display_h;
        glfwGetFramebufferSize(window, &display_w, &display_h);
        glViewport(0, 0, display_w, display_h);
        glClearColor(clear_color.x * clear_color.w, clear_color.y * clear_color.w, clear_color.z * clear_color.w, clear_color.w);
        glClear(GL_COLOR_BUFFER_BIT);
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
        glfwSwapBuffers(window);
#pragma endregion
    }

    // Cleanup
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();

    glfwDestroyWindow(window);
    glfwTerminate();
    XCloseDisplay(dpy);
    return 0;
}




#pragma clang diagnostic pop